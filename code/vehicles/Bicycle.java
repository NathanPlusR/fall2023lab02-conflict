package vehicles;

public class Bicycle
{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle (String manufacturer, int numberGears, double maxSpeed)
    {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer()
    {
        return this.manufacturer;
    }

    public int getnumberGears()
    {
        return this.numberGears;
    }

    public double getmaxSpeed()
    {
        return this.maxSpeed;
    }

    public String toString()
    {
        String s = "";
        s+= "Manufacturer " + this.manufacturer;
        s+= ", Number of Gears " + this.numberGears;
        s+= ",  MaxSpeed: " + this.maxSpeed;
        return s;
    }
}