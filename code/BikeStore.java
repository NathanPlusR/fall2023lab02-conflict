import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args)
    {
        Bicycle[] bikes = new Bicycle[4];
        
        bikes[0] = new Bicycle("HeyHay", 4, 9);
        bikes[1] = new Bicycle("Bestieco", 2, 4);
        bikes[2] = new Bicycle("TruthyBikes", 10, 10);
        bikes[3] = new Bicycle("Specialized", 9, 3);

        for (int i = 0; i < 4; i++)
        {
            System.out.println(bikes[i]);
        }
    }
}
